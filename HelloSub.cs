using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloSub : MonoBehaviour
{
    public string myName = "Sub";

    public int num = 10;
    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"Hello Sub Start, {myName} = {num}");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
